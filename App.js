import React, { useState, useEffect } from 'react';
import { StatusBar } from 'expo-status-bar';
import moment from 'moment';

import {
  Modal,
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  TouchableOpacity,
  Switch,
  Image
} from 'react-native';
import DatePicker, { getFormatedDate } from 'react-native-modern-datepicker';
import * as SQLite from 'expo-sqlite';

export default function App() {
  const db = SQLite.openDatabase('CapstoneJournal.db');
  const [isLoading, setIsLoading] = useState(true);
  const [openDatePicker, setOpenDatePicker] = useState(false);
  const [date, setDate] = useState('');
  const [isEnabled, setIsEnabled] = useState(true);
  const [text, setText] = useState('Public');
  const [openMood, setOpenMood] = useState(false);
  const [selectedMoodText, setSelectedMoodText] = useState('');
  const [title, setTitle] = useState('');
  const [message, setMessage] = useState('');

  const today = new Date();
  const startDate = getFormatedDate(today.setDate(today.getDate()), 'YYYY/MM/DD');

  useEffect(() => {
    // Create the "journal" table if it doesn't exist
    db.transaction((tx) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS journal (id INTEGER PRIMARY KEY AUTOINCREMENT, Type TEXT, Date TEXT, Mood TEXT, Title TEXT, Message TEXT)',
        [],
        () => {
          console.log('Table created successfully!');
          setIsLoading(false);
        },
        (txObj, error) => {
          console.log('Error creating table:', error);
          setIsLoading(false);
        }
      );
    });
  }, []);

  const fetchJournalEntries = () => {
    db.transaction((tx) => {
      tx.executeSql(
        'SELECT * FROM journal ORDER BY id DESC LIMIT 1',
        null,
        (txObj, resultSet) => {
          const entry = resultSet.rows.item(0);
          console.log('Submitted Entry:', entry);
        },
        (txObj, error) => console.log(error)
      );
    });
  };

  function toggleSwitch() {
    if (isEnabled) {
      setText('Private');
    } else {
      setText('Public');
    }
    setIsEnabled((previousState) => !previousState);
  }

  function handleDatePress() {
    setOpenDatePicker(!openDatePicker);
  }

  function handleChange(propDate) {
    setDate(propDate);
  }

  function handleMoodPress(text) {
    setSelectedMoodText(text);
    setOpenMood(true);
  }
  function handleSendButtonPress() {
    if (isEnabled) {
      setOpenMood(true);
    } else {
      const formattedDate = moment(date, 'YYYY/MM/DD').format('YYYY-MM-DD');
      
      // Set the default mood to an empty string
      const selectedMood = isEnabled ? selectedMoodText : '';
  
      // Insert the journal entry into the SQLite database
      db.transaction(
        (tx) => {
          tx.executeSql(
            'INSERT INTO journal (Type, Date, Mood, Title, Message) VALUES (?, ?, ?, ?, ?)',
            [text, formattedDate, selectedMood, title, message],
            (txObj, resultSet) => {
              console.log('Journal entry saved successfully!');
              // Clear input fields and reset state
              setDate('');
              setTitle('');
              setMessage('');
              setSelectedMoodText('');
            },
            (txObj, error) => {
              console.log('Error saving journal entry:', error);
            }
          );
        },
        (error) => console.log('Transaction error:', error)
      );
      fetchJournalEntries();
    }
  }
  function handleSendButtonPressPublic() {
    // Save the selected mood and close the modal
    setOpenMood(false);
  
    // Save the journal entry to the database
    const formattedDate = moment(date, 'YYYY/MM/DD').format('YYYY-MM-DD');
    const selectedMood = selectedMoodText;
  
    // Insert the journal entry into the SQLite database
    db.transaction(
      (tx) => {
        tx.executeSql(
          'INSERT INTO journal (Type, Date, Mood, Title, Message) VALUES (?, ?, ?, ?, ?)',
          [text, formattedDate, selectedMood, title, message],
          (txObj, resultSet) => {
            console.log('Journal entry saved successfully!');
            // Clear input fields and reset state
            setDate(startDate);
            setTitle('');
            setMessage('');
            setSelectedMoodText('');
          },
          (txObj, error) => {
            console.log('Error saving journal entry:', error);
          }
        );
      },
      (error) => console.log('Transaction error:', error)
    );
    fetchJournalEntries();
  }

  if (isLoading) {
    return (
      <View style={styles.container}>
        <Text>Loading...</Text>
      </View>
    );
  }
  return (
    <View style={styles.container}>
      <Text>JOURNAL</Text>
      <Text style={{ fontWeight: 'bold', margin: 20 }}>{text}</Text>
      <View style={styles.toggle}>
        <Switch
          trackColor={{ false: 'gray', true: 'red' }}
          thumbColor={isEnabled ? '#f4f3f4' : '#f4f3f4'}
          onValueChange={toggleSwitch}
          value={isEnabled}
        />
      </View>
      <TouchableOpacity onPress={handleDatePress}>
        <Text>{date}</Text>
      </TouchableOpacity>

      <Modal
        animationType="slide"
        transparent={true}
        visible={openDatePicker}
        onRequestClose={() => setOpenDatePicker(false)}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <DatePicker
              mode="calendar"
              selected={date}
              minimumDate={startDate}
              onDateChange={handleChange}
            />
            <TouchableOpacity onPress={handleDatePress}>
              <Text>Close</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      <StatusBar style="auto" />

      <TextInput
        style={styles.txtStyle}
        placeholder="Title"
        value={title}
        onChangeText={setTitle}
      />

      <TouchableOpacity onPress={() => setOpenMood(true)}>
        <Text>Select Mood</Text>
      </TouchableOpacity>

      <Modal
        animationType="slide"
        transparent={true}
        visible={openMood && isEnabled}
        onRequestClose={() => setOpenMood(false)}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text>Mood Scale</Text>
            <View style={styles.moodContainer}>
              <TouchableOpacity onPress={() => handleMoodPress('Terrible')}>
                <View style={styles.moodItem}>
                  <Image style={styles.mood} source={require('./assets/1.png')} />
                  {selectedMoodText === 'Terrible' && <Text style={styles.moodText}>Terrible</Text>}
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => handleMoodPress('Sad')}>
                <View style={styles.moodItem}>
                  <Image style={styles.mood} source={require('./assets/2.png')} />
                  {selectedMoodText === 'Sad' && <Text style={styles.moodText}>Sad</Text>}
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => handleMoodPress('Neutral')}>
                <View style={styles.moodItem}>
                  <Image style={styles.mood} source={require('./assets/3.png')} />
                  {selectedMoodText === 'Neutral' && <Text style={styles.moodText}>Neutral</Text>}
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => handleMoodPress('Happy')}>
                <View style={styles.moodItem}>
                  <Image style={styles.mood} source={require('./assets/4.png')} />
                  {selectedMoodText === 'Happy' && <Text style={styles.moodText}>Happy</Text>}
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => handleMoodPress('Great')}>
                <View style={styles.moodItem}>
                  <Image style={styles.mood} source={require('./assets/5.png')} />
                  {selectedMoodText === 'Great' && <Text style={styles.moodText}>Great</Text>}
                </View>
              </TouchableOpacity>
            </View>
            <Button title="Close" onPress={() => setOpenMood(false)} />
            <Button title="Save" onPress={handleSendButtonPressPublic} />
          </View>
        </View>   
      </Modal>

      <View>
        <TextInput
          style={{ height: 500, borderWidth: 1, marginBottom: 10, paddingHorizontal: 10 }}
          multiline={true}
          numberOfLines={5}
          textAlignVertical="top"
          placeholder="Type your message..."
          value={message}
          onChangeText={setMessage}
        />
        <Button title="Save" onPress={handleSendButtonPress} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 50,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    width: '90%',
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  toggle: {
    position: 'absolute',
    top: 10,
    right: 10,
    padding: 50,
  },
  txtStyle: {
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    marginBottom: 20,
    marginTop: 20,
  },
  moodContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  moodItem: {
    alignItems: 'center',
  },
  mood: {
    width: 50,
    height: 50,
  },
  moodText: {
    textAlign: 'center',
  }
});
